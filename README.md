# Eater

This application will help you to find a good place where you can eat. I did this application for Upfluence Interview.

## Architectural

I splitted the application in 2 parts. One for the frontend and an another one for the backend.

The backend is pretty simple, just a single route to retrieve food trucks around a position (latitude and longitude).

The frontend is done with React and Typescript.

I decided to keep the SanFrancisco API, the backend si just a "proxy". With the SF API, I can search data around a point, it's exactly what I need. I don't need to add more complexity on the backend. For the frontend, I decided to use Google Maps. If cost is an issue, we can change the provider and use for example OpenMaps. Google Maps is a good choice for me because we can use the autocomplete feature to find a location and the maps feature to display trucks on the map.

## Backend

The backend is done with golang 1.14 (simple, fast and secure).

You can start the backend with `go run main.go` and go [here](http://localhost:8000). I added monitoring with Prometheus (we can create custom metrics if needed). I also added some logs with logrus. Logs level can be changed with the flaf `-v`.

Here you can find the description of each route on the backend.

### /api/foodtrucks

You can pass 2 query parameters:

- `lat` it's the latitude where you want to center your each
- `lon` it's the longitude where you want to center your each

Response:

```json
[
  {
    "objectid": "string",
    "applicant": "string",
    "facilitytype": "string",
    "cnn": "string",
    "locationdescription": "string",
    "address": "string",
    "blocklot": "string",
    "block": "string",
    "lot": "string",
    "permit": "string",
    "status": "string",
    "fooditems": "string",
    "latitude": "string",
    "longitude": "string",
    "schedule": "string",
    "approved": "string",
    "received": "string",
    "priorpermit": "string",
    "expirationdate": "string",
    "location": {
      "latitude": "string",
      "longitude": "string",
      "human_address": "string"
    }
  }
]
```

### /api/foodtrucks/{objectid}

Return one foodtruck

Response:

```json
{
  "objectid": "string",
  "applicant": "string",
  "facilitytype": "string",
  "cnn": "string",
  "locationdescription": "string",
  "address": "string",
  "blocklot": "string",
  "block": "string",
  "lot": "string",
  "permit": "string",
  "status": "string",
  "fooditems": "string",
  "latitude": "string",
  "longitude": "string",
  "schedule": "string",
  "approved": "string",
  "received": "string",
  "priorpermit": "string",
  "expirationdate": "string",
  "location": {
    "latitude": "string",
    "longitude": "string",
    "human_address": "string"
  }
}
```

### /metrics

Expose metrics from Golang application to be used with prometheus.

## Frontend

The frontend project was created with create-react-app. I use React because I already had experiences and it's pretty fast to create an application.

I decided to don't use any library to manage google map. I didn't find an official wrapper for Google Maps Location & Google Maps. If I'm in a team, I'll maybe ask to the team what they think about that and share pros/cons.

## Available Scripts

In the project directory, you can run:

- `yarn start` to start the application. You can then navigate [here](http://localhost:3000/)
- `yarn test` to run test of your application
- `yarn build` to build the application in the `build` folder

## Security

CORS are enabled for all location. For a production application, it's better to secure that to only the production domain.

## Scalability

The frontend don't need to be scaled. We can serve files with a CDN to have High Availability (for example with CloudFront and S3). Even if the number of data point increase, we'll never display thousands points in a circle of 2km. If it's the case, we can maybe reduce the area to only one kilometer for example.

The backend can have several optimisation if we need to scale. The main bottleneck here it's the San Francisco API. If this API don't limit us, we can just add a load balancer to be able to serve more request (and duplicate golang instance). If SF API limits us (maximum number of query by IP, points, ...), then we can:

- Save data locally. I'm not sure we need to have new FoodTruck in real time. We can maybe load foodtrucks once per day and save them somewhere.
- Somewhere can be a SQL Database (Postgres) or NoSQL (MongoDB) for example. In my opinion, it's better here to use ElasticSearch.
- Thanks to ElasticSearch, we can natively search by distance and have a simple ElasticSearch cluster. ElasticSearch is also simplier to manage, upgrade and backup/restore.
- FInally, we can implement cache : we can save for example in redis the result of a query of a location for one day with a key like `md5("lat" + "lng")` and/or precalculate some popular result. But this improvement, it's really if we need to scale a LOT. In most case, ES cluster should be enough.

One last thing we can do to improve performance is to use the detail route in the backend `/api/foodtrucks/<objectid>`. This route will return the full detail of a FoodTruck. The `/api/foodtrucks` route will return only the id of the food truck and the location. On the frontend, we can call `/api/foodtrucks` when we want to search foodtrucks and call `/food/foodtrucks/<objectid>` only when we click on the marker on the map. Thanks to that we can reduce the load on the network and improve performance of the frontend.

## What to improve

Of course, the application is very simple here. If it's not for a test, I would like to :

- add tests on the frontend (need to mock Google Maps API)
- add a configuration mecanism for all timeout, URL and token api
