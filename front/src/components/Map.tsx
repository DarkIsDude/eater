import React from 'react'
import styled from 'styled-components'
import { Location, Truck } from '../model'

const Container = styled.div`
    flex: 1;
    display: flex;
    justify-content: center;
    align-items: center;
`

interface Props {
  location?: Location
}

interface State {
  error?: Error
}

export class Map extends React.Component<Props, State> {
    map: google.maps.Map|null = null

    initGoogleMap(element: HTMLDivElement|null) {
      if (!element || !window.google) return

      this.map = new window.google.maps.Map(element, {
        zoom: 15,
        center: this.props.location,
        mapTypeControl: false,
        panControl: false,
        zoomControl: true,
        streetViewControl: false,
      })

      this.loadMarkers()
    }

    loadMarkers() {
      fetch(`http://localhost:8000/api/foodtrucks?lat=${this.props.location?.lat}&lon=${this.props.location?.lng}`)
        .then(response => response.json())
        .then(trucks => trucks.forEach((truck: Truck) =>  this.addMarker(truck)))
        .catch((e) => {
          this.setState({ error: e })
        })
    }

    addMarker(truck: Truck) {
      if (!this.map) return
      const map:google.maps.Map = this.map

      const contentString = `
        <h4>${truck.objectid} ${truck.applicant}</h4>
        <p><a href="${truck.schedule}">schedule</a></p>
        <ul>
            <li>facilitytype: ${truck.facilitytype}</li>
            <li>cnn: ${truck.cnn}</li>
            <li>locationdescription: ${truck.locationdescription}</li>
            <li>address: ${truck.address}</li>
            <li>blocklot: ${truck.blocklot}</li>
            <li>block: ${truck.block}</li>
            <li>lot: ${truck.lot}</li>
            <li>permit: ${truck.permit}</li>
            <li>fooditems: ${truck.fooditems}</li>
            <li>dayshours: ${truck.dayshours}</li>
        </ul>`


      const infowindow = new google.maps.InfoWindow({
        content: contentString,
      })

      const marker = new google.maps.Marker({
        position: { lat: parseFloat(truck.latitude), lng: parseFloat(truck.longitude) },
        map,
      })

      marker.addListener('click', () => {
        infowindow.open(map, marker)
      })
    }

    render() {
      if (!this.props.location) {
        return <Container>Place select a place first</Container>
      }

      if (this.state?.error) {
        console.error(this.state.error)
        return <Container>Please refresh the application, we couldn't reach backend</Container>
      }


      return <Container ref={(e) => this.initGoogleMap(e)} />
    }
}
