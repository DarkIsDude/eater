import React from 'react'
import styled from 'styled-components'
import { Location } from '../model'

const Container = styled.div`
    display: flex;
    padding: 1rem;
    align-items: center;
`

const Input = styled.input`
    flex: 1;
    border-radius: 0.5rem;
    border: 1px solid black;
    padding: 0.5rem;
    margin-left: 1rem;
`

interface Props {
    setLocation: (place: Location) => void
}

export class Search extends React.Component<Props> {
    autocomplete: React.RefObject<HTMLInputElement>|null = null

    constructor(props: Props) {
      super(props)
      this.autocomplete = React.createRef<HTMLInputElement>()
    }

    componentDidMount() {
      if (!this.autocomplete?.current || !window.google) return

      const autocomplete = new window.google.maps.places.Autocomplete(this.autocomplete.current)
      autocomplete.addListener('place_changed', () => {
        const location = autocomplete.getPlace().geometry?.location
        if (!location) return
        this.props.setLocation({ lat: location.lat(), lng: location.lng() })
      })
    }

    render() {
      return <Container>Find FoodTruck near: <Input placeholder="Enter a location" type="text" ref={this.autocomplete} /></Container>
    }
}
