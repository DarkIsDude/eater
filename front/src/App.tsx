import React from 'react'
import { Map } from './components/Map'
import { Search } from './components/Search'
import styled from 'styled-components'
import { Location } from './model'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
`

const MapWrapper = styled(Map)`
  flex: 1;
`

interface Props {}

interface State {
  location?: Location
}

class App extends React.Component<Props, State> {
  setLocation(location: Location) {
    this.setState({
      location
    })
  }

  render() {
    return (
      <Container>
        <Search setLocation={(place) => this.setLocation(place)} />
        <MapWrapper location={ this.state?.location }/>
      </Container>
    )
  }
}

export default App
