export interface Location {
    lat: number
    lng: number
}

export interface Truck {
    objectid: string
    applicant: string
    schedule: string
    facilitytype: string
    cnn: string
    locationdescription: string
    address: string
    blocklot: string
    block: string
    lot: string
    permit: string
    fooditems: string
    dayshours: string
    latitude: string
    longitude: string
}
