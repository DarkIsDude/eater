package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestFoodTrucksHandlerWithoutParams(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/foodtrucks", nil)
	assert.NoError(t, err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(FoodTrucksHandler)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, rr.Result().StatusCode, http.StatusBadRequest, "handler returned wrong status code")
}

func TestFoodTrucksHandlerWithEmptyParameters(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/foodtrucks", nil)
	assert.NoError(t, err)

	q := req.URL.Query()
	q.Add("lat", "")
	q.Add("lon", "")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(FoodTrucksHandler)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, rr.Result().StatusCode, http.StatusBadRequest, "handler returned wrong status code")
}

func TestFoodTrucksHandlerWithStringParameters(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/foodtrucks", nil)
	assert.NoError(t, err)

	q := req.URL.Query()
	q.Add("lat", "azerty")
	q.Add("lon", "wxcvb")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(FoodTrucksHandler)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, rr.Result().StatusCode, http.StatusInternalServerError, "handler returned wrong status code")
}

func TestFoodTrucksHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/foodtrucks", nil)
	assert.NoError(t, err)

	q := req.URL.Query()
	q.Add("lat", "37.7749295")
	q.Add("lon", "-122.4194155")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(FoodTrucksHandler)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, rr.Result().StatusCode, http.StatusOK, "handler returned wrong status code")
}

func TestFoodTruckHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/foodtrucks/1336139", nil)
	assert.NoError(t, err)

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.HandleFunc("/api/foodtrucks/{objectid}", FoodTruckHandler)
	router.ServeHTTP(rr, req)

	assert.Equal(t, rr.Result().StatusCode, http.StatusOK, "handler returned wrong status code")
	assert.Equal(t, "{\"objectid\":\"1336139\",\"applicant\":\"Brazuca Grill\",\"facilitytype\":\"Truck\",\"cnn\":\"10048000\",\"locationdescription\":\"OTIS ST: GOUGH ST \\\\ MCCOPPIN ST to 13TH ST \\\\ DUBOCE AVE \\\\ HWY 101 NORTHBOUND RAMP \\\\ MISSION ST (100 - 199)\",\"address\":\"150 OTIS ST\",\"blocklot\":\"3513008\",\"block\":\"3513\",\"lot\":\"008\",\"permit\":\"19MFF-00052\",\"status\":\"APPROVED\",\"fooditems\":\"Cold Truck: Sandwiches: Noodles:  Pre-packaged Snacks: Candy: Desserts Various Beverages\",\"latitude\":\"37.7706833950426\",\"longitude\":\"-122.420879561399\",\"schedule\":\"http://bsm.sfdpw.org/PermitsTracker/reports/report.aspx?title=schedule\\u0026report=rptSchedule\\u0026params=permit=19MFF-00052\\u0026ExportPDF=1\\u0026Filename=19MFF-00052_schedule.pdf\",\"approved\":\"2020-01-22T00:00:00.000\",\"received\":\"2019-07-09\",\"priorpermit\":\"1\",\"expirationdate\":\"2021-01-15T00:00:00.000\",\"location\":{\"latitude\":\"37.7706833950426\",\"longitude\":\"-122.420879561399\",\"human_address\":\"{\\\"address\\\": \\\"\\\", \\\"city\\\": \\\"\\\", \\\"state\\\": \\\"\\\", \\\"zip\\\": \\\"\\\"}\"}}\n", rr.Body.String(), "The body should be the same as expected")
}
