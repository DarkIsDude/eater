package main

import (
	"eater/pkg/monitor"
	"eater/pkg/service"
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// FoodTrucksHandler it's the handler to find all food trucks around a specific location
func FoodTrucksHandler(w http.ResponseWriter, r *http.Request) {
	lat := r.URL.Query().Get("lat")
	lon := r.URL.Query().Get("lon")

	if lat == "" {
		logrus.Debug("lat parameter can't be empty")
		http.Error(w, "lat parameter can't be empty", http.StatusBadRequest)
		return
	}

	if lon == "" {
		logrus.Debug("lon parameter can't be empty")
		http.Error(w, "lon parameter can't be empty", http.StatusBadRequest)
		return
	}

	trucks, err := service.FindNeariestTrucks(lat, lon)
	if err != nil {
		logrus.WithError(err).Error("couldn't encode the answer")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")

	encoder := json.NewEncoder(w)
	err = encoder.Encode(trucks)
	if err != nil {
		logrus.WithError(err).Error("couldn't find trucks")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// FoodTruckHandler it's the handler to find all food trucks around a specific location
func FoodTruckHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	objectid := vars["objectid"]

	truck, err := service.FindTruck(objectid)
	if err, ok := err.(*service.TruckNotFound); ok {
		logrus.WithError(err).Error("couldn't find the truck")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	} else if err != nil {
		logrus.WithError(err).Error("couldn't find the truck - unknown error")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	encoder := json.NewEncoder(w)
	err = encoder.Encode(truck)
	if err != nil {
		logrus.WithError(err).Error("couldn't encode the answer")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func main() {
	router := mux.NewRouter()
	monitor := monitor.New()

	var verbose = flag.Bool("v", false, "Enable verbose mode")
	if *verbose {
		logrus.SetLevel(logrus.DebugLevel)
	}

	router.HandleFunc("/api/foodtrucks", monitor.WithMetrics("foodtrucks", FoodTrucksHandler))
	router.HandleFunc("/api/foodtrucks/{objectid}", monitor.WithMetrics("foodtruck", FoodTruckHandler))
	router.Handle("/metrics", promhttp.Handler())

	logrus.Info("Ready to listen port 8000")
	srv := &http.Server{
		Handler:      router,
		Addr:         "0.0.0.0:8000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
