package service

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// FoodTruck is a food truck from the SF API
type FoodTruck struct {
	ID                  string `json:"objectid"`
	Applicant           string `json:"applicant"`
	FacilityType        string `json:"facilitytype"`
	CNN                 string `json:"cnn"`
	LocationDescription string `json:"locationdescription"`
	Address             string `json:"address"`
	BlockLot            string `json:"blocklot"`
	Block               string `json:"block"`
	Lot                 string `json:"lot"`
	Permit              string `json:"permit"`
	Status              string `json:"status"`
	FoodItems           string `json:"fooditems"`
	Latitude            string `json:"latitude"`
	Longitude           string `json:"longitude"`
	Schedule            string `json:"schedule"`
	Approved            string `json:"approved"`
	Received            string `json:"received"`
	PriorPermit         string `json:"priorpermit"`
	ExpirationDate      string `json:"expirationdate"`
	Location            struct {
		Latitude     string `json:"latitude"`
		Longitude    string `json:"longitude"`
		HumanAddress string `json:"human_address"`
	} `json:"location"`
}

const apiURL = "https://data.sfgov.org/resource/rqzj-sfat.json"

var client = http.Client{
	Timeout: 5 * time.Second,
}

// FindNeariestTrucks return all first 10 trucks near the lat and lon
func FindNeariestTrucks(lat string, lon string) ([]FoodTruck, error) {
	req, err := http.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't create request")
	}

	q := req.URL.Query()
	q.Add("$where", fmt.Sprintf("within_circle(location, %s, %s, 2000)", lat, lon))
	req.URL.RawQuery = q.Encode()

	logrus.WithField("url", req.URL.String()).Info("ready to call")
	res, err := client.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't process the get request")
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't read the answer")
	}

	var trucks []FoodTruck
	err = json.Unmarshal(data, &trucks)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't decode the answer")
	}

	logrus.WithField("count", len(trucks)).Info("found trucks before filter")

	var trucksFiltered []FoodTruck = make([]FoodTruck, 0)
	for _, truck := range trucks {
		if truck.Status == "APPROVED" {
			trucksFiltered = append(trucksFiltered, truck)
		}
	}

	logrus.WithField("count", len(trucksFiltered)).Info("found trucks after filter")
	return trucksFiltered, nil
}

// FindTruck return one truck with the objectid
func FindTruck(objectid string) (FoodTruck, error) {
	var truck FoodTruck

	req, err := http.NewRequest("GET", apiURL, nil)
	if err != nil {
		return truck, errors.Wrap(err, "couldn't create request")
	}

	q := req.URL.Query()
	q.Add("objectid", objectid)
	req.URL.RawQuery = q.Encode()

	logrus.WithField("url", req.URL.String()).Info("ready to call")
	res, err := client.Do(req)
	if err != nil {
		return truck, errors.Wrap(err, "couldn't process the get request")
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return truck, errors.Wrap(err, "couldn't read the answer")
	}

	var trucks []FoodTruck
	err = json.Unmarshal(data, &trucks)
	if err != nil {
		return truck, errors.Wrap(err, "couldn't decode the answer")
	}

	if len(trucks) == 0 {
		return truck, NewTruckNotFound(objectid)
	}

	truck = trucks[0]
	return truck, nil
}
