package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindNeariestTrucks(t *testing.T) {
	// San Francisco
	trucks, err := FindNeariestTrucks("37.7749295", "-122.4194155")

	assert.NoError(t, err)
	assert.Len(t, trucks, 96)
	assert.Equal(t, "1332938", trucks[0].ID)

	// Montpellier
	trucks, err = FindNeariestTrucks("43.610769", "3.876716")

	assert.NoError(t, err)
	assert.Len(t, trucks, 0)
}
