package service

import "fmt"

// NewTruckNotFound Create a new truck not found error
func NewTruckNotFound(objectid string) error {
	return &TruckNotFound{objectid}
}

// TruckNotFound is the error triggered when we can't find the food truck
type TruckNotFound struct {
	objectid string
}

func (e *TruckNotFound) Error() string {
	return fmt.Sprintf("Truck %s not found", e.objectid)
}
