package monitor

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Monitor contains monitor metrics
type Monitor struct {
	requestCounter    *prometheus.CounterVec
	durationHistogram *prometheus.HistogramVec
}

// New create a new monitor and registers the version in prometheus
func New() Monitor {
	monitor := Monitor{
		requestCounter: promauto.NewCounterVec(prometheus.CounterOpts{
			Name: "eater_requests_total",
			Help: "A counter for requests to the wrapped handler.",
		}, []string{"code", "handler"}),
		durationHistogram: promauto.NewHistogramVec(prometheus.HistogramOpts{
			Name: "eater_request_duration_seconds",
			Help: "A histogram of latencies for requests.",
		}, []string{"handler", "code"}),
	}

	return monitor
}

// WithMetrics wraps a handler and gets metrics about the request duration and the number requests
func (monitor *Monitor) WithMetrics(name string, next http.HandlerFunc) http.HandlerFunc {
	return promhttp.InstrumentHandlerDuration(
		monitor.durationHistogram.MustCurryWith(prometheus.Labels{"handler": name}),
		promhttp.InstrumentHandlerCounter(
			monitor.requestCounter.MustCurryWith(prometheus.Labels{"handler": name}),
			next,
		),
	)
}
